export const timedLogout = () => {
  setTimeout(() => {
    localStorage.clear()
    window.open('https://binar-rental-server.herokuapp.com/auth/logout', '_self')
  }, 1000)
}
