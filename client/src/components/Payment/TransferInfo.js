import React, { useState } from 'react'
import { Box, Card, CardContent, Typography, Chip, Button, ButtonGroup } from '@mui/material'

const TransferInfo = () => {
  // eslint-disable-next-line no-unused-vars
  const [view, setView] = useState('')

  const handleChange = (e) => {
    setView(e.target.value)
  }

  const buttons = [
    <Button
      key="one"
      value="bca"
      onClick={handleChange}
      sx={{ justifyContent: 'start', color: '#000000', py: 2 }}
    >
      <Chip
        label="BCA"
        variant="outlined"
        sx={{ width: 'max-content', borderRadius: '8px', mr: 1 }}
      />
      BCA Transfer
    </Button>,
    <Button
      key="two"
      value="bni"
      onClick={handleChange}
      sx={{ justifyContent: 'start', color: '#000000', py: 2 }}
    >
      <Chip
        label="BNI"
        variant="outlined"
        sx={{ width: 'max-content', borderRadius: '8px', mr: 1 }}
      />
      BNI Transfer
    </Button>,
    <Button
      key="three"
      value="mandiri"
      onClick={handleChange}
      sx={{ justifyContent: 'start', color: '#000000', py: 2 }}
    >
      <Chip
        label="Mandiri"
        variant="outlined"
        sx={{ width: 'max-content', borderRadius: '8px', mr: 1 }}
      />
      Mandiri Transfer
    </Button>,
  ]
  return (
    <>
      <Card
        variant="outlined"
        sx={{
          maxWidth: '780px',
          borderRadius: '8px',
        }}
      >
        <Box component="div" sx={{ m: '20px' }}>
          <CardContent
            sx={{
              p: '8px',
              '&:last-child': {
                pb: '0',
              },
            }}
          >
            <Typography sx={{ fontSize: '16px', fontWeight: 'bold' }}>
              Pilih Bank Transfer
            </Typography>
            <Typography variant="body1" sx={{ mt: 2, mb: 3 }}>
              Kamu bisa membayar dengan transfer melalui ATM, Internet Banking atau Mobile Banking
            </Typography>

            <Box
              sx={{
                display: 'flex',
              }}
            >
              <ButtonGroup
                orientation="vertical"
                aria-label="vertical outlined button group"
                fullWidth
                variant="text"
                sx={{ textAlign: 'left' }}
              >
                {buttons}
              </ButtonGroup>
            </Box>
          </CardContent>
        </Box>
      </Card>
    </>
  )
}

export default TransferInfo
